FROM python:3.12.3-alpine3.19

WORKDIR /app

COPY ./fibonacci.py fibonacci.py
COPY ./requirements.txt requirements.txt

RUN pip install -r requirements.txt

CMD [ "python", "/app/fibonacci.py" ]

EXPOSE 5000