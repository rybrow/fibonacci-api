## Goals

A list of goals I would like to achieve in this 3 hour time limit. (this was written before I started)

- Basic implementation of the Fibbonacci REST API
- Unit testing to validate the core logic of the fibbonacci sequence calculation
- Smoketests used to validate the functionality of the REST endpoint
- Docker Build and distribution using Gitlab CI
- Basic Local Deployment using docker compose
- (Time Permitted) Helm Chart with example values.yaml

## Technologies

I would like to use the following technologies for this practical assessment

- *Source Control:* Git / GitLab
- *CI/CD:* Gitlab CI
- Rest API: Python (Flask)
- Unit Tests: PyTest
- Smoke Tests: Postman locally and maybe curl for CI/CD (this is a simple service, probably doesnt need a super complex test automation setup...)
- *Containerization:* Docker / Docker Compose / Kubernetes / Helm