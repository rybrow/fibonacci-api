# Fibonacci API
A small Flask Rest API which returns the nth number in the fibonacci sequence.

## Assumptions
Instructions of this project assume you have the a working knowledge of the following and that they are installed on your machine:

- [python3](https://www.python.org/downloads/)
- [docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/)

## Building and Running the Fibonacci API Locally
There are two options for this, either python or docker

### Run with Python
install requirements with

    pip install -r requirements.txt

start the app with

    python fibonacci.py

### Run with Docker
If you prefer to use docker only, the default docker-compose.yml is set up to build from the local context

    docker-compose build
    docker-compose up

## Testing the API
Once the API is running it should be accessible at "http://localhost:5000"

The base url doesnt do anything, as of yet, but you can find the nth number in the fibonacci sequence by the nth number as a path parameter: "http://localhost:5000/{nth}"

Examples:
- http://localhost:5000/0
- http://localhost:5000/1
- http://localhost:5000/9001
- http://localhost:5000/10

### Unit Tests

This project utilizes the pytest library for unit testing, the tests can be found in the "test_fibonacci.py" file

To run the unit tests, install the requirements with:

    pip install -r requirements.txt

and use the following command to execute the tests

    python -m pytest


# Operational Considerations

## CI/CD & Containerization:

This project produces the following docker image

    registry.gitlab.com/rybrow/fibonacci-api:latest

### Running the Release Images from Docker Compose

This repo has two docker-compose files, a development and release one.

to spin up the release images produced by the Gitlab CI/CD pipeline run the below

    docker-compose -f .\docker-compose-release.yml up

### Gitlab CI Pipeline

I've set this project up to use a Gitlab CI/CD Pipeline with the following stages

- validate: for unit testing, compilation, and things like linting, security scanning etc.
- package: building the docker image and publishing a prerelease version
- test: a job to run the unit tests from
- publish: publishes the prerelease version as "latest", on a Tag Pipeline it will also publish the tagged version which will be used in production.

This CI/CD pipeline currently doesnt have any merge request / dev branch functionality but If this was a production grade project, we would have guard rails on the main branch and validations (unit tests, sonarqube, container scanning) that get run on a merge request / dev branch etc.

The "latest" version is considered unstable and should not be used in production. Only use the versions created by tag pipelines in a production environment.

Ideally the "latest" version would get promoted to some form of shared development environment and from there product owners / testers would perform validations and determine whether these get released and promoted to production.

## Logging

I am utilizing the Flask log levels for this project, in the docker compose files or in any containerized environment (ECS, Kubernetes etc.), you can set a LOG_LEVEL environment variable to set the different log levels

Log levels used are
- DEBUG
- INFO (Default)
- ERROR

## Monitoring / Autoscaling for high loads

In a production environment, I would set up a metrics endpoint on the container to output things like CPU usage and Memory limits, Requests recieved etc.

From this endpoint we can use tools like prometheus to get more insights into whats going on inside the containers, and determine if they are under a high load etc.

I would then use these metrics and set up some form of autoscaling, in kubernetes it would be a horizontal pod autoscaler, probably linked to the CPU usage, or a Custom Metric which tells us how many requests per second the container has recieved.
