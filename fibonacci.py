from flask import Flask
import os

upper_limit = 20577
lower_limit = 0

app = Flask(__name__)

def fibonacci(n):
    if n < lower_limit:
        raise ValueError(f"Bad Request: Provide a positive number (n cannot be below {lower_limit})")

    if n > upper_limit: # Python cannot convert anything higher than this to a string response
        raise ValueError(f"Bad Request: Sorry, try a lower number (n cannot be above {upper_limit})")

    if n <= 1:
        app.logger.info(f"Skipping calculation and returning {n} as N <= 2")
        return n
    else:
        app.logger.info(f"Beginning Fibonacci Sequence Calculation for N={n}")
        n_minus_1 = 1
        n_minus_2 = 0
        nth_value = 0

        for index in range(2, n+1):
            nth_value = n_minus_1 + n_minus_2
            n_minus_2 = n_minus_1
            n_minus_1 = nth_value
            app.logger.debug(f"{index}: {nth_value}")

        app.logger.info(f"Finished Fibonacci Sequence Calculation for N={n}")
        app.logger.info(f"Result: {nth_value}")
        return nth_value


@app.route("/<int:n>")
def return_nth_fibonacci_number(n):
    app.logger.info(f"Request Recieved, calclating fibonacci sequence value for N={n}")
    try:
        nth = fibonacci(n)
        return str(nth), 200
    except ValueError as value_error:
        app.logger.error(value_error)
        return str(value_error), 400
    except Exception as exception:
        app.logger.error(exception)
        return str(exception), 500

if __name__ == '__main__':
   app.logger.setLevel(os.getenv("LOG_LEVEL", "INFO"))
   app.run(host='0.0.0.0')
