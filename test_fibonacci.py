
import pytest
from fibonacci import fibonacci, upper_limit, lower_limit

def test_fibonacci_returns_0_with_n0():
    assert fibonacci(0) == 0

def test_fibonacci_returns_1_with_n1():
    assert fibonacci(1) == 1

def test_fibonacci_returns_error_with_n_below_lower_limit():
    with pytest.raises(ValueError):
        fibonacci(lower_limit - 1)

def test_fibonacci_returns_error_with_n_above_upper_limit():
    with pytest.raises(ValueError):
        fibonacci(upper_limit + 1)

def test_fibonacci_returns_correct_values():
    # Grabbed first 20 from wikipedia to assert
    assert fibonacci(2) == 1
    assert fibonacci(3) == 2
    assert fibonacci(4) == 3
    assert fibonacci(5) == 5
    assert fibonacci(6) == 8
    assert fibonacci(7) == 13
    assert fibonacci(8) == 21
    assert fibonacci(9) == 34
    assert fibonacci(10) == 55
    assert fibonacci(11) == 89
    assert fibonacci(12) == 144
    assert fibonacci(13) == 233
    assert fibonacci(14) == 377
    assert fibonacci(15) == 610
    assert fibonacci(16) == 987
    assert fibonacci(17) == 1597
    assert fibonacci(18) == 2584
    assert fibonacci(19) == 4181